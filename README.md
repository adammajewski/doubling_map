
Doubling map
* [wikipedia](https://en.wikipedia.org/wiki/Dyadic_transformation) 
* [wikibooks](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/def_cqp#Doubling_map)
* [Marc McClure](https://www.marksmath.org/classes/Fall2017ChaosAndFractals/chaos_and_fractals/section-chaos_and_doubling.html)


# Tasks
* iterate the angle t
* compute preperiod/period of the angle under doubling map 
  * use ratio of (big) integers t = n/m 
  * find pattern in the binary string 
    * [codereview.stackexchange](https://codereview.stackexchange.com/questions/220313/preperiodic-periodic-or-aperiodic-binary-string)
    * [rosetta code: Rep-string](https://rosettacode.org/wiki/Rep-string)
* convert 
  * decimal fraction to floating point binary number ( in the formatted form)
  * real decimal to real binary 
    * [bc](https://rosettacode.org/wiki/Decimal_floating_point_number_to_binary#bc)



# files
* [m.c](m.c) = forward and backward iteration of doubling map with itinerary ( array of integers, one integer for 1 symbol)
* [b.c](b.c) = bits array 
* to do : forward and backward iteration of doubling map with itinerary. Here itinerary is also an array of integers  but one integer is for 32 symbols (bits array) 


# Iteration 

## Using double 

### bad example
```bash
forward iteration of doubling map
i = 0 t = 0.100000
i = 1 t = 0.200000
i = 2 t = 0.400000
i = 3 t = 0.800000 wrap
i = 4 t = 0.600000 wrap
i = 5 t = 0.200000

backward iteration of doubling map
i = 5 t = 0.200000
i = 4 t = 0.100000
i = 3 t = 0.050000
i = 2 t = 0.025000
i = 1 t = 0.012500
i = 0 t = 0.006250
```

### good example 

#### 1/3

```bash
forward iteration of doubling map
t0 = 0.333333
t1 = 0.666667 wrap
t2 = 0.333333
t3 = 0.666667 wrap
t4 = 0.333333
t5 = 0.666667

backward iteration of doubling map = halving map 
t5 = 0.666667
t4 = 0.333333 unwrap
t3 = 0.666667
t2 = 0.333333 unwrap
t1 = 0.666667
t0 = 0.333333


 results 
t0 = 0.333333
t5 = 0.666667
tr = 0.333333
dt = fabs(t0- tr) = 0.000000

itinerary:
itinerary[0] = 0 
itinerary[1] = 1 
itinerary[2] = 0 
itinerary[3] = 1 
itinerary[4] = 0 

decimal 0.333333 has binary expansion = 0.01010

program works good !
```

#### 1/7

```bash
forward iteration of doubling map
t0 = 0.142857
t1 = 0.285714
t2 = 0.571429 wrap
t3 = 0.142857
t4 = 0.285714
t5 = 0.571429 wrap
t6 = 0.142857
t7 = 0.285714
t8 = 0.571429

backward iteration of doubling map = halving map 
t8 = 0.571429
t7 = 0.285714
t6 = 0.142857 unwrap
t5 = 0.571429
t4 = 0.285714
t3 = 0.142857 unwrap
t2 = 0.571429
t1 = 0.285714
t0 = 0.142857


results 
t0 = 0.142857
t8 = 0.571429
tr = 0.142857
dt = fabs(t0- tr) = 0.000000

itinerary:
itinerary[0] = 0 
itinerary[1] = 0 
itinerary[2] = 1 
itinerary[3] = 0 
itinerary[4] = 0 
itinerary[5] = 1 
itinerary[6] = 0 
itinerary[7] = 0 

decimal 0.142857 has binary expansion = 0.00100100

```



# technical note
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)


# Git

``` git 
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/doubling_map.git
git add .
git commit -m "Initial commit"
git push -u origin master
```


local git repo : c/julia/phase/modulo_test/



