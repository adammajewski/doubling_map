/*
http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/1-C-intro/bit-array.html

 gcc b.c -Wall
 Shun Yan Cheung,
 
 modified  
 
 
 
*/




#include <stdio.h>


/* ========================================
   Bit Operations as macros
   ======================================== */
#define SetBit(A,k)     ( A[(k/32)] |= (1 << (k%32)) ) // set bit to 1 
#define ClearBit(A,k)   ( A[(k/32)] &= ~(1 << (k%32)) ) // set bit to 0 
#define TestBit(A,k)    ( A[(k/32)] & (1 << (k%32)) ) // if bit is 1 then true else false 

#define LENGTH 10  // length of the array with 32-bit cells 

int main(  )
{
   int Bits[LENGTH];
   int l; // number of the array cell 
   int i; // bit number
   int iMax = LENGTH*32; // length of the array in bits 

   for ( l = 0; l < LENGTH; l++ )
      Bits[l] = 0;                    // Clear the bit array

   printf("Set bit positions 100, 200 and 300\n");
   SetBit( Bits, 100 );               // Set 3 bits
   SetBit( Bits, 200 );
   SetBit( Bits, 300 );


   // Check if SetBit() works:

   for ( i = 0; i < iMax; i++ )
      if ( TestBit(Bits, i) )
         printf("Bit %d was set !\n", i);

   printf("\nClear bit positions 200 \n");
   ClearBit( Bits, 200 );

   // Check if ClearBit() works:
         
   //      
   printf("Bits = ");      
   for ( i = 0; i < iMax; i++ )
      if ( TestBit(Bits, i) )
         printf("1");
         else printf("0"); 
       
   printf("\n");             
         
  return 0;       
         
}
